

public class Point {

    double x = 0.0;
    double y = 0.0;

    public Point() {}

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public String toString() {
        // (x,y)
        return String.format("(%2.2f, %2.2f)", this.x, this.y);
    }

    public boolean equals(Point p) {
        return this.x == p.x && this.y == p.y;
    }

    public double distance(Point p) {
        // raiz((x1  - x2)**2 + (y1 - y2)**2)
        double px = p.x;
        double py = p.y;
        double d = Math.sqrt((Math.pow(this.x - px, 2)) + (Math.pow(this.y - py, 2)));
        return d;
    }

}
