

public class Triangle {

    Edge a1;
    Edge a2;
    Edge a3;

    public Triangle(Point a, Point b, Point c) {
        this.a1 = new Edge(a,b);
        this.a2 = new Edge(b,c);
        this.a3 = new Edge(c,a);
    }

    public String toString() {
        return String.format("AB: %s BC: %s CA: %s", this.a1, this.a2, this.a3);
    }

    public double areaTrianCoord() {
        return 0.0;
    }

}
