

import java.lang.IllegalArgumentException;
public class Edge {

    Point p1 = new Point();
    Point p2 = new Point();

    // TODO: dos puntos iguales no hacen una arista

    public Edge(Point p1, Point p2) {
        if (p1.equals(p2))
            throw new IllegalArgumentException("Los puntos son iguales");
        this.p1 = p1;
        this.p2 = p2;
    }

    public String toString() {
        String sp1 = this.p1.toString();
        String sp2 = this.p2.toString();
        return String.format("[%s - %s]", sp1, sp2);
    }

    // TODO: middlePoint
    public Point middlePoint() {
        //x =  (x1 + x2) / 2
        //y = (y1 + y2) / 2
        double x = (p1.x + p2.x) / 2;
        double y = (p1.y + p2.y) / 2;
        return new Point(x,y);
    }
}
