


public class Main {

    public static void main(String[] args) {
        Point p1 = new Point(2.5, 8);
        System.out.println(p1);
        Point p2 = new Point();
        System.out.println(p2);
        System.out.println(p1.equals(p1));
        System.out.println(p1.equals(p2));
        Point p3 = new Point(2.5, 8);
        System.out.println(p3);
        System.out.println(p1.equals(p3));

        Point p4 = new Point();
        Point p5 = new Point(0, 10);

        System.out.println(p4.distance(p5));

        Edge e1 = new Edge(p4, p5);
        System.out.println(e1);

        // Edge e2 = new Edge(p4, p4);
        // System.out.println(e2);

        System.out.println(e1.middlePoint());

        Triangle t = new Triangle(p3, p4, p5);
        System.out.println(t);
    }
}
